""" A trainer recipe to train Sementatic segmentation work
"""
import torch
from seger.loss import get_loss
from seger.optims import get_optim, get_schedulers
from seger.dataloaders import MaskDataset
from seger.vis_lib.visdom_vis import VisdomLinePlotter

import numpy as np

class semantic_trainer():
    def __init__(self, cfg, model):
        self.model = model
        self.cfg = cfg
        
        if self.cfg["device"]["use_gpu"]:
            self.model = self.model.cuda()
        
        if self.cfg["device"]["use_data_parallel"]:
            self.model = torch.nn.DataParallel(self.model, device_ids= self.cfg["device"]["device_ids"])
        
        loss_kwargs = {
            "num_classes": self.cfg["data"]["num_classes"]
        }
        self.criterion = get_loss(self.cfg["train"]["loss"], loss_kwargs)
        
        self.optimizer = get_optim(self.cfg["train"]["optim"],
                                  self.model,
                                  self.cfg["train"]["optim_kwargs"])
        self.scheduler = get_schedulers(self.cfg["train"]["sched_name"],
                                       self.optimizer,
                                       self.cfg["data"]["total_images"],
                                       self.cfg["train"]["batch_size"],
                                       self.cfg["train"]["sched_kwargs"])
        
        if self.cfg["device"]["use_visdom"]:
            self.plot = VisdomLinePlotter(env_name = self.cfg["model_stuff"]["exp_name"])
        
        self.epochs = 0
        self.trainset = MaskDataset(self.cfg, True)
        self.trainloader = torch.utils.data.DataLoader(self.trainset, batch_size=self.cfg["train"]["batch_size"], shuffle=True, num_workers=4)
        
    def trainer(self, ep):
        """ For each epoch, we need to train equally between all the img sizes,
        """
        torch.set_grad_enabled(True)
        self.model.train()
        total_iterations = len(self.trainloader)*ep

        loss_tracker = []
        for num, k in enumerate(self.trainloader):
            inputs, targets = k
            self.scheduler.optimizer.zero_grad()
            lr = self.scheduler.fix_lr()
            inputs, targets = k 
            if self.cfg["device"]["use_gpu"]:
                inputs = inputs.cuda()
                targets = targets.float().cuda()
            outputs = self.model(inputs)
            #outputs = torch.rand((outputs.size[0], 3, 416, 416))
            final_loss = self.criterion(outputs, targets)
            final_loss.backward()
            #torch.nn.utils.clip_grad_norm_(self.model.parameters(),10)
            self.scheduler.optimizer.step()
            print("Iter: {}, loss:{}".format(total_iterations+num, np.array(float(final_loss.cpu()))))


            if self.cfg["device"]["use_visdom"]:
                self.plot.plot("final_loss_tracker", "training", total_iterations+num, np.array(float(final_loss.cpu())))
                self.plot.plot("lr", "training", total_iterations+num, np.array(lr))

            loss_tracker.append(float(final_loss.cpu()))

        loss_tracker = np.mean(loss_tracker)
        if self.cfg["device"]["use_visdom"]:
            self.plot.plot("total_loss_epoch_level", "training", ep, np.array(float(loss_tracker)))
        self.epochs+=1

        return loss_tracker 
    
    def save_model(self, score):
        save = {}
        save["model_weights"] = self.model.state_dict() 
        save["epochs"] = self.epochs
        save["scheduler"] = self.scheduler.state_dict()
        save["best_score"] = score 
        name =self.cfg["model_stuff"]["exp_name"]+ "/" + "_"+str(self.epochs)+"_"+str(score)+".pth"
        torch.save(save,  name)
        return name
    
    def load_model(self):
        save = torch.load(self.cfg["train"]["resume_training"])
        if self.cfg["device"]["use_gpu"]:
            self.model.load_state_dict(save["model_weights"])
        else:
            self.model.load_state_dict(OrderedDict((k.split(".", 1)[1], v) for k, v in save["model_weights"].items()))
        self.scheduler.load_state_dict(save["scheduler"])
        self.epochs = save["epochs"]
        return save["best_score"]

            
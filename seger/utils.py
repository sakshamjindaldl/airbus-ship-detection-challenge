""" A list of seger utils implemented to make life simpler
"""

import matplotlib.pyplot as plt
import numpy as np

def plot_scatter(x,y,x_label="",y_label="",header="",figure_size=(5,5),savename = None, s=None):
    plt.figure(figsize=figure_size)
    plt.scatter(x,y,s=s)
    plt.xlabel(x_label, fontsize = 20)
    plt.ylabel(y_label, fontsize = 20)
    plt.title(header, fontsize = 20)
    if savename is not None:
        plt.savefig(savename)
    else:
        plt.show()
        
def mask_2_box(img):
    rows = np.any(img, axis=1)
    cols = np.any(img, axis=0)
    rmin, rmax = np.where(rows)[0][[0, -1]]
    cmin, cmax = np.where(cols)[0][[0, -1]]
    return rmin, rmax, cmin, cmax

def rle_encode(img):
    '''
    img: numpy array, 1 - mask, 0 - background, shape: [H, W]
    Returns run length as string formated
    '''
    pixels = img.flatten()
    pixels = np.concatenate([[0], pixels, [0]])
    runs = np.where(pixels[1:] != pixels[:-1])[0] + 1
    runs[1::2] -= runs[::2]
    return ' '.join(str(x) for x in runs)


def rle_decode(mask_rle, shape=(768, 768)):
    """
    mask_rle: run-length as string formated(start length)
    shape: (height, width)
    
    returns numpy array, 1 - mask, 0 - background
    """
    s = mask_rle.split()
    starts, lengths = [np.asarray(x, dtype=int) for x in (s[0:][::2], s[1:][::2])]
    starts -= 1
    ends = starts + lengths
    img = np.zeros(shape[0]*shape[1], dtype=np.uint8)
    for lo, hi in zip(starts, ends):
        img[lo:hi] = 1
    return img.reshape(shape).T  # Needed to align to RLE direction


#def mask_2_box(cfg, mask_rle, shape=(768, 768)):
#    img = rle_decode(mask_rle, shape)
#    #ToDo -> extract a tight bounding box. make sure that only one bounding box is extracted from each mask
#    return img

def imshow(images, masks, imgs_per_row=4):
    """
    images - stack of images
    masks - stack of respective masks
    """
    mean = list(cfg["train"]["mean"])
    std = list(cfg["train"]["std"])
    for img in images:
        for i in range(3):
            img[i] = img[i]*std[i] + mean[i]
    pil_convertor = torchvision.transforms.ToPILImage(mode='RGB')
    pil_images = [ pil_convertor(img) for img in images ]
    batches = math.ceil(len(pil_images)/float(imgs_per_row))
    for i in range(batches):
        imgs = pil_images[i*imgs_per_row:(i+1)*imgs_per_row]
        lab = labels[i*imgs_per_row:(i+1)*imgs_per_row]
        fig, ax = plt.subplots(nrows=1, ncols=len(imgs), sharex="col", sharey="row", figsize=(4*(len(imgs)),4), squeeze=False)
        for i, img in enumerate(imgs):    
            ax[0,i].imshow(img)
            ax[0,i].set_title(lab[i].item())
""" Networks to train deep neural networks
"""
__author__ = ["PrakashJay"]

import torch
from seger.nets.unet import UNet
from seger.nets.segnet import segnet
import torchvision.models as models

def get_network(input_channels, num_classes, model_name, kwargs):
    if  model_name == "unet":
        print("Initializing UNET")
        model = UNet(input_channels, num_classes)
    elif model_name == 'segnet':
        print("Initializing segnet  ")
        model = segnet(num_classes, input_channels, True)
        vgg16 = models.vgg16(pretrained=True)
        model.init_vgg16_params(vgg16)
    else:
        raise NotImplementedError("We haven't implemented this network architectures yet. Please raise a pull request now. Thank you")

    return model
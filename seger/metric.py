"""F0 Score and F2 score
"""

import numpy as np
import math
from skimage.morphology import label


def get_metric(metric_name):
    if metric_name== "f_score":
        return fscore_single_image
    else:
        raise NotImplementedError("This metric is not implemented yet")


def fscore_single_image(y_true_in, y_pred_in,beta= 2, print_table = False):
    """As described here https://www.kaggle.com/c/airbus-ship-d etection#evaluation
    
    y_true_in: A numpy array of shape [H, W]. This is actual
    y_pred_in: A numpy array of shape [H, W]. This is predicted
    
    """
    #labels = label(y_true_in > threshold)
    #y_pred = label(y_pred_in > threshold)
    
    labels = y_true_in
    y_pred = y_pred_in
    
    true_objects = len(np.unique(labels))
    pred_objects = len(np.unique(y_pred))

    intersection = np.histogram2d(labels.flatten(), y_pred.flatten(), bins=(true_objects, pred_objects))[0]

    # Compute areas (needed for finding the union between all objects)
    area_true = np.histogram(labels, bins = true_objects)[0]
    area_pred = np.histogram(y_pred, bins = pred_objects)[0]
    area_true = np.expand_dims(area_true, -1)
    area_pred = np.expand_dims(area_pred, 0)

    # Compute union
    union = area_true + area_pred - intersection

    # Exclude background from the analysis
    intersection = intersection[1:,1:]
    union = union[1:,1:]
    union[union == 0] = 1e-9

    # Compute the intersection over union
    iou = intersection / union

    # Precision helper function
    def precision_at(threshold, iou):
        matches = iou > threshold
        true_positives = np.sum(matches, axis=1) == 1   # Correct objects
        false_positives = np.sum(matches, axis=0) == 0  # Missed objects
        false_negatives = np.sum(matches, axis=1) == 0  # Extra objects
        tp, fp, fn = np.sum(true_positives), np.sum(false_positives), np.sum(false_negatives)
        return tp, fp, fn

    # Loop over IoU thresholds
    prec = []
    threshold_ = []
    if print_table:
        print("Thresh\tTP\tFP\tFN\tPrec.")
    beta2 = math.pow(2, beta)
    for t in np.arange(0.5, 1.0, 0.05):
        tp, fp, fn = precision_at(t, iou)
        if ((1+beta2)*tp) + fp + (beta2*fn) > 0:
            p = ((1+beta2)*tp) / ( ((1+beta2)*tp) + fp + (beta2*fn))
        else:
            p = 0
        if print_table:
            print("{:1.3f}\t{}\t{}\t{}\t{:1.3f}".format(t, tp, fp, fn, p))
        prec.append(p)
        threshold_.append(t)
    
    if print_table:
        print("AP\t-\t-\t-\t{:1.3f}".format(np.mean(prec)))
    return np.sum(prec)/len(threshold_)

def fscore(y_true_in, y_pred_in, threshold=0.5, beta=2, print_table=False):
    """ metric cal for all the images
    y_true_in: [N, H, W]
    """
    pass
    
    
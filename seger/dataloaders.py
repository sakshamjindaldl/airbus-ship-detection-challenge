""" Datagenerator to train the model

The list file looks like this

img.png mask.png
"""
import os
import sys
import random
import numpy as np

import torch
import torch.utils.data as data
import torchvision.transforms as transforms

from PIL import Image

from seger.utils import rle_decode


def resize(img, mask, shape):
    img = img.resize(shape, Image.BILINEAR)
    if mask is not None:
        mask= mask.resize(shape, Image.BILINEAR)
    else:
        mask = 0
    return img, mask

def read_semantic_rle_format(list_file):
    """ If the output is given in rle format. 
    """
    fnames, fmasks = list(), list()
    with open(list_file) as f:
            lines = f.readlines()
            num_samples = len(lines)

    for line in lines:
        splited = line.strip().split(" ", 1)
        fnames.append(splited[0])
        fmasks.append(splited[1])
    return num_samples, fnames, fmasks

def read_semantic_mask_format(list_files):
    """ If the output is given in the image format 
    """
    fnames, fmasks = list(), list()
    with open(list_file) as f:
            lines = f.readlines()
            num_samples = len(lines)

    for line in lines:
        splited = line.strip().split()
        fnames.append(splited[0])
        fmasks.append(splited[1])
    return num_samples, fnames, fmasks



class MaskDataset(data.Dataset):
    def __init__(self, cfg, train):
        '''
        Important Args:
          cfg: (str) config file which contains all the params
          train: (boolean) train or test.
        '''
        self.cfg = cfg
        self.train = train
        self.input_channels = cfg["data"]["input_channels"]
        self.ouput_channels = cfg["data"]["num_classes"]
        if self.train:
            list_file = self.cfg["data"]["train_data"]
        else:
            list_file = self.cfg["data"]["valid_data"]
        self.input_size = self.cfg["train"]["input_size"]
        self.transform = transforms.Compose([transforms.ToTensor()])
        
        if self.cfg["data"]["output_format"] == "mask":
            self.num_samples, self.fnames, self.fmask = read_semantic_mask_format(list_file)
        elif self.cfg["data"]["output_format"] == "rle":
            self.num_samples, self.fnames, self.fmask = read_semantic_rle_format(list_file)
        else:
            raise ValueError("The format is not yet implemented. Please check the code")

        self.seen = 0

        

    def __getitem__(self, idx):
        '''Load image.
        Args:
          idx: (int) image index.
        Returns:
          img: (tensor) image tensor.
          loc_targets: (tensor) location targets.
          cls_targets: (tensor) class label targets.
        '''
        # Load image and boxes.
                
        self.seen += 1
        fname = self.fnames[idx]
        fmask = self.fmask[idx]
        #print(fname)
        #os.path.join(self.root, fname)
        img = Image.open(self.cfg["data"]["data_loc"]+fname)
        if self.input_channels == 3:
            if img.mode != 'RGB':
                img = img.convert('RGB')
                    
        
        if self.cfg["data"]["output_format"] == "mask":
            mask = Image.open(self.cfg["data"]["data_loc"]+fmask)
        elif self.cfg["data"]["output_format"] == "rle":
            mask = rle_decode(fmask,  (img.size[1], img.size[0])) * 255
            mask = Image.fromarray(mask) ## converting numpy array to PIL image so that the down pipeline remains the same
        
        if self.train:
            ## Some pre-processing after sometime 
            if self.cfg["train"]["resize"]:
                img, mask = resize(img, mask, (self.input_size, self.input_size))
            img = self.transform(img)
            mask = self.transform(mask)
        else:
            img = self.transform(img)
            mask = self.transform(img)
        
        if self.cfg["data"]["mask_type"] == "16bit":
            mask = mask.byte()/255
        else:
            mask = mask

        return img, mask
        # Data augmentation
        
    
    def __len__(self):
        return 1000#len(self.fnames)
    
    @staticmethod
    def prep_image(img_loc, inp_dim):
        """
        Prepare image for inputting to the neural network. 

        Returns a Variable 
        
        img_loc: str: Location of the image
        inp_dim: tuple: resize dimension. The aspect ratio is not taken care off.
        """
        img = Image.open(img_loc)
        if img.mode != 'RGB':
            img = img.convert('RGB')
        
        shape = (img.size[1], img.size[0]) #(h, w)
        
        img_, _ = resize(img, None, (inp_dim, inp_dim))
        img_tensor = transforms.ToTensor()(img_)
        img_tensor = img_tensor.unsqueeze(0)
        return img_tensor, shape
    
    

#if __name__ == '__main__':
    #ToDo
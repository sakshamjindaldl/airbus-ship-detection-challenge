""" Networks to train deep neural networks
"""
__author__ = ["PrakashJay"]

import torch
import numpy as np
import torch.nn as nn
import torch.nn.functional as F

def get_loss(loss, kwargs):
    if loss == "bcewithlogitsloss":
        print("BCE with Logits Loss")
        return torch.nn.BCEWithLogitsLoss()
    elif loss == "focalloss":
        print("Focal Loss")
        raise ValueError("Focal Loss construction is going on.")
    elif loss == "dice":
        print("Dice Loss single class")
        return Dice(kwargs["num_classes"])
    else:
        raise NotImplementedError("This is not implemented yet.. Raise pull request")

def dice_loss(inputs, target):
    smooth = 1.
    iflat = inputs.view(-1)
    tflat = target.view(-1)
    intersection = (iflat * tflat).sum()

    return ((2. * intersection + smooth) /
            (iflat.sum() + tflat.sum() + smooth))

def dice_loss_multiclass(inputs, target, num_classes):
    sums = 0
    dice =0
    inputs = np.argmax(inputs.data.cpu().numpy(),axis = 1)
    target = target.data.cpu().numpy()

    for i in range(num_classes):
        label_input = (inputs == i+1).astype(np.uint8)
        label_target = (target == i+1).astype(np.uint8)

        label_input = torch.from_numpy(label_input)
        label_target = torch.from_numpy(label_target)
        if i == 2 :
            dice = dice_loss(label_input,label_target)
        sums = sums + dice_loss(label_input, label_target)
    dice_scores = {"total" : sums/(num_classes -1), "shingle" : dice}
    return dice_scores


class Dice(nn.Module):
    """Implementing dice loss function
    
    ## Important Links:
    - http://forums.fast.ai/t/understanding-the-dice-coefficient/5838/2
    """
    def __init__(self, num_classes):
        super().__init__()
        self.num_classes = num_classes
    
    def forward(self, inputs, targets):
        inputs = F.sigmoid(inputs)
        batch_size = inputs.size(0)
        if self.num_classes > 1:
            score = dice_loss_multiclass(inputs, targets, self.num_classes)
            score = 1- (score["total"].sum()/batch_size)
        else:
            score = dice_loss(inputs, targets)
            score = 1 - (score.sum() / batch_size)
        return score
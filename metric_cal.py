""" Calculate the metric. 
"""

import argparse
import json

from seger.dataloaders import read_semantic_rle_format, read_semantic_mask_format, MaskDataset
from seger.metric import get_metric
from seger.utils import rle_encode, rle_decode



parser = argparse.ArgumentParser(description="Inference on dataset")
parser.add_argument("-cfg", "--cfg_loc", default="cfgs/airbus.json")
parser.add_argument("-loc", "--data_loc", default=None)
parser.add_argument("-save_loc", "--save_loc",default="predict.txt")
parser.add_argument("-thresh", "--threshold",default=0.1, type=float)

args = parser.parse_args()

print("[Loading the config file...]")
with open(args.cfg_loc) as fp:
    cfg = json.load(fp)
print(cfg)
print(args.cfg_loc, args.save_loc)


def cal_metric(cfg, original_txt, predicted_txt):
    metric = get_metric(cfg["train"]["metric"])
    
    if cfg["data"]["output_format"] == "mask":
        org_num_samples, org_fnames, org_masks = read_semantic_mask_format(original_txt)
        pred_num_samples, pred_fnames, pred_masks = read_semantic_mask_format(predicted_txt)
    elif cfg["data"]["output_format"] == "rle":
        org_num_samples, org_fnames, org_masks = read_semantic_rle_format(original_txt)
        pred_num_samples, pred_fnames, pred_masks = read_semantic_rle_format(predicted_txt)
    else:
        raise ValueError("Required format is not present")
    #assert pred_num_samples == org_num_samples
    
    for num in range(len(pred_fnames)):
        org = org_masks[num]
        pred = pred_masks[num]
        
        if cfg["data"]["output_format"] == "mask":
            raise ValueError("Not implemented yet") 
        elif cfg["data"]["output_format"] == "rle":
            org_mask = rle_decode(org, (cfg["train"]["input_size"], cfg["train"]["input_size"]))
            pred_mask = rle_decode(pred, (cfg["train"]["input_size"], cfg["train"]["input_size"]))
        score = metric(org_mask, pred_mask, cfg["train"]["metric_kwargs"]["beta_score"])
        print(score)
        
    
cal_metric(cfg, args.data_loc, args.save_loc)     

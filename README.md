# ksegerchal
Contains code for semantic, panoptic and instance segmentation work.

## Public datasets to test code 


## Code structuring for sematic challenge
- seg_analyzer
- src 
    - nets
    - dataloaders
    - metrics
    - agumentor
    - image_utils

- semantic segmentation research papers implementation
- [ ] UNet
- [ ] SegNet
- [ ] FCN

## Loss functions to implement
- [ ] Focal Loss 
- [ ] Dice Loss

## DataLoaders work

## 




## References 
- https://github.com/mrgloom/awesome-semantic-segmentation

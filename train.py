import json
import argparse
import os
import time
import torch

from seger.dataloaders import MaskDataset
from seger.nets.networks import get_network
from trainer import semantic_trainer

parser = argparse.ArgumentParser(description="Image to mask work")
parser.add_argument("-cfg", "--cfg_loc", default="cfgs/airbus.json")

args = parser.parse_args()

with open(args.cfg_loc, "r") as fp:
    cfg = json.load(fp)

print(cfg)
        
print("Model loader ....")
model = get_network(cfg["data"]["input_channels"], cfg["data"]["num_classes"], cfg["arch"]["arch_name"], cfg["arch"]["arch_kwargs"])

if cfg["train"]["use_pretrained_weights_loc"] is not None:
    print("Loading pretrained weights")
    model.load_weights(cfg["train"]["use_pretrained_weights_loc"])


print("Initializing the trainer......")
model_trainer = semantic_trainer(cfg, model)

best_loss = float("Inf")

if not os.path.exists(cfg["model_stuff"]["exp_name"]):
    os.makedirs(cfg["model_stuff"]["exp_name"])

if cfg["train"]["resume_training"] is not None:
    print("resuming training")
    best_loss = model_trainer.load_model()
    print("best_previous_loss: {}".format(best_loss))


overall_clock = time.clock()


for ep in range(model_trainer.epochs, cfg["train"]["epochs"]):
    start = time.clock()
    print("[Epoch {}]".format(ep))
    train_loss = model_trainer.trainer(ep)
    save_loc = model_trainer.save_model(train_loss)
    print("train_loss: {}, time_taken: {}, saved_at: {}".format(train_loss, 
                                                                time.clock() - start, 
                                                                save_loc))
print("Training completed")
print("total_training_time: {}".format(time.clock()- start))

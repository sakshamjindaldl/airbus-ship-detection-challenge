""" Inference file. This will take the cfg, weights_file and image as input

CUDA_VISIBLE_DEVICES=0 python eval.py -wgt airbus_exp1/_1_0.05897029706649482.pth -loc /data/kaggle/airbus/semantic_valid.txt -thresh 0.5
"""
import argparse
import os
import json
import torch
import numpy as np

from seger.dataloaders import read_semantic_rle_format, read_semantic_mask_format, MaskDataset
from seger.metric import get_metric
from seger.nets.networks import get_network
from seger.utils import rle_encode, rle_decode



parser = argparse.ArgumentParser(description="Inference on dataset")
parser.add_argument("-cfg", "--cfg_loc", default="cfgs/airbus.json")
parser.add_argument("-wgt", "--model_weights", default=None)
parser.add_argument("-loc", "--data_loc", default=None)
parser.add_argument("-save_loc", "--save_loc",default="predict.txt")
parser.add_argument("-thresh", "--threshold",default=0.1, type=float)

args = parser.parse_args()

print("[Loading the config file...]")
with open(args.cfg_loc) as fp:
    cfg = json.load(fp)
print(cfg)
print(args.model_weights, args.cfg_loc, args.save_loc)

print("(Loading the model)")
model = get_network(cfg["data"]["input_channels"], cfg["data"]["num_classes"], cfg["arch"]["arch_name"], cfg["arch"]["arch_kwargs"])

if cfg["device"]["use_gpu"]:
    model = model.cuda()

print("(Loading weights...)")
load_weights = torch.load(args.model_weights)
model.load_state_dict(load_weights["model_weights"])

write_predictions = open(args.save_loc, "w")


def get_post_process(out, threshold):
    """ Output tensor from the network is taken and thresholds are applied as per the project requirement. This is specific to airbus requirement.
    """
    out = torch.nn.functional.sigmoid(out)
    #print(out)
    out[out >= threshold] = 1
    out[out < threshold] = 0
    return out

def get_output_format(out, req_format):
    """Given a numpy array this will output the labels in required format
    """
    if req_format == "rle":
        out = rle_encode(out)
        if len(out) == 0:
            print("No output")
            out = "0"
    elif req_format == "mask":
        pass 
    else:
        raise ValueError("Format Not found... Get_Output_format traceback")
    return out
    

for num, i in enumerate(open(args.data_loc, "r")):
    img = i.rsplit(" ")[0]
    print(img)
    img_tensor, _ = MaskDataset.prep_image(cfg["data"]["data_loc"]+img, cfg["train"]["input_size"])
    if cfg["device"]["use_gpu"]:
        img_tensor = img_tensor.cuda()
    out = model.predict(img_tensor).squeeze(0).squeeze(0)
    out = get_post_process(out, args.threshold)
    output_format = get_output_format(out.cpu().numpy(), cfg["data"]["output_format"])
    write_predictions.write("{} {} \n".format(img, output_format))
    if num > 100:
        break
write_predictions.close()
print("File Created......")